const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
const request = require("request");

const port = 3000;
const eaApiUrl = "http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars"

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/app.html"));
});

app.get("/shows", (req, res) => {
  request({
    uri: eaApiUrl
  }).pipe(res);
});

app.listen(port, () => console.log(`hello`));
