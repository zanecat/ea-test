'use strict';

const AppNav = () => (
  <nav class="navbar">
    <div class="nav-content">Cars and Shows</div>
  </nav>
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  noResultMessage = '';

  handleClick = async e => {
    e.preventDefault();
    this.noResultMessage = '';
    const response = fetch('/shows')
      .then(res => res.json())
      .catch(() => {
        this.noResultMessage = 'Oops, the server has gone, try again!';
      });
    const showCars = this.extractCars(await response);
    this.setState({ data: showCars });
  };

  extractCars = showCars => {
    const cars = [];
    if (showCars) {
      showCars.forEach(show => {
        show.cars.forEach(showCar => {
          // see if make already exists in current cars
          const indexOfMake = cars.findIndex(c => {
            return c.make === showCar.make;
          });

          if (indexOfMake === -1) {
            cars.push({
              make: showCar.make,
              models: [{ name: showCar.model, shows: [show.name] }]
            });
          } else {
            // see if model already exists in current car make
            const indexOfModel = cars[indexOfMake].models.findIndex(model => {
              return model.name === showCar.model;
            });

            if (indexOfModel === -1) {
              cars[indexOfMake].models.push({
                name: showCar.model,
                shows: [show.name]
              });
            } else {
              cars[indexOfMake].models[indexOfModel].shows
                ? cars[indexOfMake].models[indexOfModel].shows.push(show.name)
                : (cars[indexOfMake].models[indexOfModel].shows = [show.name]);
            }
          }
        });
      });
    } else {
      this.noResultMessage = 'Oops, the server has gone, try again!';
    }

    return cars;
  };

  carInfo = car => (
    <li>
      {car.make}
      <ul>{car.models.map(this.modelInfo)}</ul>
    </li>
  );

  modelInfo = model => (
    <li>
      {model.name || <i class="placeholder">model not offered</i>}
      {model.shows ? <ul>{model.shows.map(this.carShowInfo)}</ul> : ''}
    </li>
  );

  carShowInfo = show => <li>{show}</li>;

  getButtonText = () =>
    this.state.data.length > 0 ? 'Refresh' : 'Show me the cars!';

  getContent = () =>
    this.state.data.length > 0 ? (
      <ul id="car-info">{this.state.data.map(this.carInfo)}</ul>
    ) : (
      <div class="error">
        <p>{this.noResultMessage}</p>
      </div>
    );

  render() {
    return (
      <div>
        <AppNav />

        <div class="container">
          <button id="main-btn" onClick={this.handleClick}>
            {this.getButtonText()}
          </button>

          <div class="content-area">{this.getContent()}</div>
        </div>
      </div>
    );
  }
}

const domContainer = document.querySelector('#root');
ReactDOM.render(React.createElement(App), domContainer);
